Name:		rest
Version:	0.9.1
Release:	4
Summary:	A library for access to RESTful web services
License:	LGPLv2
URL:		https://www.gnome.org
Source0:	https://download.gnome.org/sources/%{name}/0.9/%{name}-%{version}.tar.xz

Patch0:		0001-rest_proxy_call_sync-bail-out-if-no-payload.patch
Patch1:		0002-Handle-some-potential-problems-in-parsing-oauth2-acc.patch
Patch2:		skip-some-failed-tests.patch

%if "0%{?product_family}" == "0"
BuildRequires:  libsoup3-devel
%else
BuildRequires:  libsoup-devel
%endif
BuildRequires:	glib2-devel libxml2-devel gobject-introspection-devel
BuildRequires:  libxslt meson json-glib-devel libadwaita-devel gtksourceview5-devel gi-docgen

%description
This library has been designed to make it easier to access web services that
claim to be "RESTful". It comprises of two parts: the first aims to make it
easier to make requests by providing a wrapper around libsoup, the second
aids with XML parsing by wrapping libxml2.

%package devel
Summary: Development files for rest
Requires: %{name} = %{version}-%{release}

%description devel
This package is the development files for rest.

%package demo
Summary: Demo application for %{name}
Requires: %{name} = %{version}-%{release}

%description demo
Demo application for %{name}.

%package_help

%prep
%autosetup -n %{name}-%{version} -p1

%build
%if "0%{?product_family}" == "0"
%meson
%else
%meson -Dsoup2=true
%endif
%meson_build


%install
%meson_install

%ldconfig_scriptlets

%check
%meson_test

%files
%defattr(-,root,root)
%doc AUTHORS
%license COPYING
%{_libdir}/librest-1.0.so.*
%{_libdir}/librest-extras-1.0.so.*
%{_libdir}/girepository-1.0/Rest-*
%{_libdir}/girepository-1.0/RestExtras-*


%files devel
%defattr(-,root,root)
%{_includedir}/rest-1.0
%{_libdir}/librest-1.0.so
%{_libdir}/librest-extras-1.0.so
%{_datadir}/gir-1.0/Rest-*
%{_datadir}/gir-1.0/RestExtras-*
%{_libdir}/pkgconfig/rest*

%files demo
%{_datadir}/applications/org.gnome.RestDemo.desktop
%{_bindir}/librest-demo

%files help
%defattr(-,root,root)
%doc README.md
%{_datadir}/doc/librest-1.0

%changelog
* Wed Jul 03 2024 zhouyihang <zhouyihang3@h-partners.com> - 0.9.1-4
- Type:requirements
- Id:NA
- SUG:NA
- DESC:use libsoup2 instead of libsoup3 for build when no libsoup3 provided

* Thu Oct 26 2023 zhouyihang <zhouyihang3@h-partners.com> - 0.9.1-3
- Type:requirements
- Id:NA
- SUG:NA
- DESC:enable test

* Sat Mar 25 2023 zhouyihang <zhouyihang3@h-partners.com> - 0.9.1-2
- Type:requirements
- Id:NA
- SUG:NA
- DESC:remove useless libs

* Thu Feb 02 2023 zhouyihang <zhouyihang3@h-partners.com> - 0.9.1-1
- Type:requirements
- Id:NA
- SUG:NA
- DESC:update rest to 0.9.1

* Wed Nov 09 2022 zhouyihang <zhouyihang3@h-partners.com> - 0.8.1-8
- Type:bugfix
- Id:NA
- SUG:NA
- DESC:xml do not crash parsing empty XML string

* Sat Nov 23 2019 openEuler Buildteam <buildteam@openeuler.org> - 0.8.1-7
- Type:bugfix
- Id:NA
- SUG:NA
- DESC:add the libxslt in buildrequires

* Tue Sep 3 2019 openEuler Buildteam <buildteam@openeuler.org> - 0.8.1-6
- Type: enhancement
- ID:   NA
- SUG:  NA
- DESC: rebuilt spec, add help package

* Tue Aug 13 2019 openEuler Buildteam <buildteam@openeuler.org> - 0.8.1-5
- Package init
